ocaml-benchmark (1.7-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Wed, 12 Feb 2025 11:54:11 +0100

ocaml-benchmark (1.6-4) unstable; urgency=medium

  * Team upload
  * Use ocaml_dune DH buildsystem

 -- Stéphane Glondu <glondu@debian.org>  Thu, 17 Aug 2023 09:29:12 +0200

ocaml-benchmark (1.6-3) unstable; urgency=medium

  * Team upload
  * Fix build with recent dune (Closes: #1041034)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 14 Jul 2023 11:58:48 +0200

ocaml-benchmark (1.6-2) unstable; urgency=medium

  * Team upload
  * Remove libpcre-ocaml-dev from Build-Depends (unused)
  * Bump Standards-Version to 4.6.0
  * Bump debian/watch version to 4

 -- Stéphane Glondu <glondu@debian.org>  Thu, 02 Dec 2021 08:46:46 +0100

ocaml-benchmark (1.6-1) unstable; urgency=medium

  * Team upload
  * Remove Hendrik from Uploaders
  * New upstream release
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Tue, 25 Aug 2020 07:14:45 +0200

ocaml-benchmark (1.3-3) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Sun, 08 Sep 2019 18:05:10 +0200

ocaml-benchmark (1.3-2) unstable; urgency=medium

  * Team upload
  * Add ocamlbuild to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Sun, 16 Jul 2017 13:17:23 +0200

ocaml-benchmark (1.3-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 3.9.6 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 04 May 2015 08:44:55 -0300

ocaml-benchmark (1.2-1) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

  [ Hendrik Tews ]
  * fix watch file and homepage
  * import new upstream version
  * bump debhelper compat level and standards version
  * add myself as uploader
  * update dependencies, Vcs fields, copyright
  * change packaging to debhelper
  * delete old patches
  * delete some unused files in debian subdir

 -- Hendrik Tews <hendrik@askra.de>  Sun, 23 Jun 2013 21:01:51 +0200

ocaml-benchmark (0.9-2) unstable; urgency=low

  * Use dh-ocaml 0.9.1 features
  * Upgrade Standards-Version to 3.8.3 (no change)

 -- Sylvain Le Gall <gildor@debian.org>  Mon, 14 Dec 2009 23:00:28 +0000

ocaml-benchmark (0.9-1) unstable; urgency=low

  * New Upstream Version
  * Remove patch 02_gettimeofday, applied upstream
  * Apply 03makedoc patch to fix documentation generation problem
  * Upgrade Standards-Version to 3.8.1 (no change)

 -- Sylvain Le Gall <gildor@debian.org>  Fri, 01 May 2009 18:35:22 +0200

ocaml-benchmark (0.8-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Sylvain Le Gall ]
  * New upstream version
  * Remove debian/control.in
  * Use dh-ocaml for generating .ocamldoc-apiref automatically
  * Remove patch 01destdir, applied upstream
  * Apply patch 02gettimeofday, to have a WALL time with a precision better
    than 1 second (replace Unix.time by Unix.gettimeofay)
  * Switch packaging to git
  * Set maintainer to Debian OCaml Maintainers
  * Upgrade Standards-Version to 3.8.0 (debian/README.source)
  * Add Homepage field to debian/control
  * Add ${misc:Depends} to dependencies
  * Upgrade debian/compat to 7 (use debian/clean)

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 07 Mar 2009 00:43:56 +0100

ocaml-benchmark (0.6-7) unstable; urgency=low

  * Build for ocaml 3.10.0
  * Move API documentation to the standard directory of OCaml

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 04 Sep 2007 00:26:45 +0200

ocaml-benchmark (0.6-6) experimental; urgency=low

  * Ooops, depends field is empty

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 14 Jul 2007 01:48:48 +0200

ocaml-benchmark (0.6-5) experimental; urgency=low

  * Upgrade debian/watch version to 3,
  * Upgrade debhelper debian/compat to 5,
  * Use CDBS for debian/rules,
  * Change email address to gildor@debian.org everywhere
  * Change watch URL to sf.net
  * Rebuild for ocaml 3.10.0

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 07 Jul 2007 23:26:25 +0200

ocaml-benchmark (0.6-4) unstable; urgency=low

  * Change my email address to gildor@debian.org,
  * Made debian/control a PHONY target,
  * Upgrade standards version to 3.7.2 (no change),

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 13 Jun 2006 08:58:34 +0200

ocaml-benchmark (0.6-3) unstable; urgency=low

  * Rebuild for OCaml 3.09.1

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Wed, 11 Jan 2006 00:01:42 +0100

ocaml-benchmark (0.6-2) unstable; urgency=low

  * Forget to update control using control.in. Add dependency on ocaml-
    findlib (Closes: #343408)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Mon, 19 Dec 2005 23:57:29 +0100

ocaml-benchmark (0.6-1) unstable; urgency=high

  * Initial Release. ( Closes: #303310 )

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Sat,  3 Dec 2005 01:55:44 +0100
